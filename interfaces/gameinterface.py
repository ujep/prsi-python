from abc import abstractmethod, ABC
from typing import Optional

from card.card import Card
from card.cardcolor import CardColor
from player import Players
from player.player import Player


class GameInterface(ABC):
    @abstractmethod
    def game_start(self):
        pass

    @abstractmethod
    def let_player_select_card(self, player: Player, card: Card, color_on_charger: CardColor, over_charge_count: int) -> Optional[Card]:
        pass

    @abstractmethod
    def show_first_card(self, card: Card):
        pass

    @abstractmethod
    def place_card(self, card: Card):
        pass

    @abstractmethod
    def add_card_to_deck(self, player: Player, card: Card):
        pass

    @abstractmethod
    def let_player_select_color(self) -> Optional[CardColor]:
        pass

    @abstractmethod
    def player_retrieved_over_charged_cards(self, player: Player, count: int):
        pass

    @abstractmethod
    def game_over(self, players: Players, round: int):
        pass

    @abstractmethod
    def current_player(self, player: Player):
        pass
