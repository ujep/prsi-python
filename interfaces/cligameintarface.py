import os
from typing import Optional

from card.card import Card
from card.cardcolor import CardColor
from card.cardtype import CardType
from interfaces.gameinterface import GameInterface
from player import Players
from player.player import Player
from translations.translation import Translation


class CLIGameInterface(GameInterface):

    def __init__(self, translation: Translation):
        self.translation = translation

    @staticmethod
    def clear_console():
        command = 'clear'
        if os.name in ('nt', 'dos'):  # If Machine is running on Windows, use cls
            command = 'cls'
        os.system(command)

    def game_start(self):
        self.clear_console()

        game_start = self.translation.translation('game_start')
        prompt_for_continue = self.translation.translation('prompt_for_continue')

        print(f'{game_start}\n\n')

        input(prompt_for_continue)

    def let_player_select_card(self, player: Player, card: Card, color_on_charger: CardColor, over_charge_count: int) -> Optional[Card]:
        is_special = card.is_special and over_charge_count is not None

        if card.is_super_special and over_charge_count is not None:
            current_color_translation = self.translation.translation('current_color')
            current_color = self.translation.card_color_name(color_on_charger)

            print(f'\n{current_color_translation}:\n{current_color}\n')
        else:
            current_card_translation = self.translation.translation('current_card')
            current_card = self.translation.card_name(card)

            print(f'\n{current_card_translation}:\n{current_card}\n')

        cards_for_select = []
        remaining_cards = []

        if card.is_super_special:
            for card1 in player.deck:
                if card1.color == color_on_charger:
                    cards_for_select.append(card1)
                else:
                    remaining_cards.append(card1)
        elif is_special:
            for card1 in player.deck:
                if card1.type == card.type:
                    cards_for_select.append(card1)
                else:
                    remaining_cards.append(card1)
        else:
            for card1 in player.deck:
                if card1.is_super_special or card.compare(card1):
                    cards_for_select.append(card1)
                else:
                    remaining_cards.append(card1)

        if len(cards_for_select) > 0:
            cards_for_select_translation = self.translation.translation('cards_for_select')

            print(f'{cards_for_select_translation}:')

            for i, card in enumerate(cards_for_select):
                print(f'{i}) {self.translation.card_name(card)}')

            print()

            if len(remaining_cards) > 0:
                remaining_cards_translation = self.translation.translation('remaining_cards')
                print(f'{remaining_cards_translation}:')

                for card in remaining_cards:
                    print(f'\t{self.translation.card_name(card)}')

            select_card_translation = self.translation.translation('select_card')

            print(f'{select_card_translation}')
            response = input(': ')

            try:
                return cards_for_select[int(response)]
            except:
                return None
        else:
            no_cards_to_play_translation = self.translation.translation('no_cards_to_play_on_overcharge' if is_special else 'no_cards_to_play')
            remaining_cards_translation = self.translation.translation('remaining_cards')

            print(f'{no_cards_to_play_translation}\n\n{remaining_cards_translation}:')

            for card in player.deck:
                print(f'\t{self.translation.card_name(card)}')

            prompt_for_continue_translation = self.translation.translation('prompt_for_continue')

            print()
            input(prompt_for_continue_translation)

            return None

    def show_first_card(self, card: Card):
        pass

    def place_card(self, card):
        pass

    def add_card_to_deck(self, player: Player, card: Card):
        pass

    def let_player_select_color(self) -> Optional[CardColor]:
        colors_for_select = self.translation.translation('colors_for_select')

        print(f'\n{colors_for_select}:')

        colors = list(CardColor)

        for i in range(len(colors)):
            print(f'{i}) {self.translation.card_color_name(colors[i])}')

        select_color = self.translation.translation('select_color')

        print(f'\n{select_color}')
        response = input(': ')

        try:
            return colors[int(response)]
        except:
            return None

    def player_retrieved_over_charged_cards(self, player: Player, count: int):
        pass

    def game_over(self, players: Players, round: int):
        self.clear_console()

        game_over = self.translation.translation('game_over')
        winner_is = self.translation.translation('winner_id')

        print(f'{game_over}\n\n{winner_is} {players.winner().name}')

    def current_player(self, player: Player):
        self.clear_console()

        string_player = self.translation.translation('player')
        plays = self.translation.translation('plays')

        print(f'{string_player} {player.name} {plays}')
