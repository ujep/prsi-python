from abc import ABC, abstractmethod

from card.card import Card
from card.cardcolor import CardColor


class Translation(ABC):
    def __init__(self, international_language_name: str, language_name: str):
        self.card_color_translations = self.card_color_translation()
        self.string_translations = self.string_translations()
        self.card_translations = self.card_translation()
        self.international_language_name = international_language_name
        self.language_name = language_name

    def card_name(self, key: Card):
        return self.card_translations.get(key)

    def card_color_name(self, key: CardColor):
        return self.card_color_translations.get(key)

    def translation(self, key: str):
        return self.string_translations.get(key)

    @abstractmethod
    def card_translation(self) -> dict[Card, str]:
        pass

    @abstractmethod
    def card_color_translation(self) -> dict[CardColor, str]:
        pass

    @abstractmethod
    def string_translations(self) -> dict[str, str]:
        pass
