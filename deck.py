from card.card import Card
from card.cards import Cards
from random import shuffle


class Deck:
    def __init__(self):
        self.cards: list[Card] = Cards.cards()
        self.remaining_cards: list[Card] = self.cards.copy()
        self.placed_cards: list[Card] = []

    def setup_deck(self):
        self.placed_cards.clear()
        self.remaining_cards.clear()
        self.remaining_cards.extend(self.cards)

        shuffle(self.remaining_cards)

    def retrieve_card(self):
        if len(self.remaining_cards) > 0:
            element = self.remaining_cards[0]
            self.remaining_cards.remove(element)

            return element
        else:
            self.reroll()

            return self.retrieve_card()

    def retrieve_cards(self, count: int):
        return [self.retrieve_card()]*count

    def reroll(self):
        self.remaining_cards.extend(self.placed_cards)
        self.remaining_cards.reverse()
        self.placed_cards.clear()

    def append_placed_card(self, card: Card):
        self.placed_cards.append(card)

