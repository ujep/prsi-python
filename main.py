# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
from card.cardtype import CardType
from game import Game
from interfaces.cligameintarface import CLIGameInterface
from player.player import Player
from translations.czech_translation import CzechTranslation


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print(list(CardType))

    lang = CzechTranslation()
    game_interface = CLIGameInterface(lang)
    players = [
        Player('Tomáš1'),
        Player('Tomáš2')
    ]

    game = Game(set(players), game_interface)

    game.prepare_game()
    game.game_start()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
