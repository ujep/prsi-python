from card.cardtype import CardType
from deck import Deck
from interfaces.gameinterface import GameInterface
from player.Players import Players
from player.player import Player
from threading import Thread


class Game:
    CARDS_PER_PLAYER = 4

    def __init__(self, players: set[Player], interface: GameInterface):
        self.players = Players(players)
        self.game_interface = interface

        self.deck = Deck()

        self.game_running = False
        self.over_charge_count = None
        self.color_on_change = None
        self.round = 0

    def prepare_game(self):
        self.deck.setup_deck()
        self.players.clear_players_decks()
        self.round = 0

        for _ in range(Game.CARDS_PER_PLAYER):
            for player in self.players.remaining_players:
                player.add_card_to_deck(self.deck.retrieve_card())

    def game_start(self):
        if not self.game_running:
            self.game_running = True
            Thread(target=self.run_game).start()

    def run_game(self):
        self.game_interface.game_start()
        current_card = self.deck.retrieve_card()

        self.game_interface.show_first_card(current_card)

        while self.players.playing_players_count() > 1:
            for player in self.players.remaining_players:
                self.game_interface.current_player(player)

                if current_card.is_special and self.over_charge_count is not None:
                    optional_selected_card = self.game_interface.let_player_select_card(player, current_card, self.color_on_change, self.over_charge_count)

                    if optional_selected_card is not None:
                        selected_card = optional_selected_card

                        if selected_card.type == CardType.__7:
                            self.over_charge_count += 1

                        player.remove_card_from_deck(selected_card)
                        self.deck.append_placed_card(selected_card)
                        self.game_interface.place_card(selected_card)
                        current_card = selected_card
                    else:
                        if current_card.type == CardType.SEVEN:
                            for card in self.deck.retrieve_cards(2*self.over_charge_count):
                                player.add_card_to_deck(card)

                            self.game_interface.player_retrieved_over_charged_cards(player, 2*self.over_charge_count)

                        self.over_charge_count = None
                else:
                    optional_selected_card = self.game_interface.let_player_select_card(player, current_card, self.color_on_change, self.over_charge_count)

                    if optional_selected_card is not None:
                        selected_card = optional_selected_card

                        if selected_card.type == CardType.MENIC:
                            self.color_on_change = self.game_interface.let_player_select_color()
                        elif selected_card.type == CardType.SEVEN:
                            self.over_charge_count = 1

                        player.remove_card_from_deck(selected_card)
                        self.deck.append_placed_card(selected_card)
                        self.game_interface.place_card(selected_card)
                        current_card = selected_card
                    else:
                        retrieved_card = self.deck.retrieve_card()
                        player.add_card_to_deck(retrieved_card)
                        self.game_interface.add_card_to_deck(player, retrieved_card)

            self.players.check_players()
            self.round += 1

        self.game_interface.game_over(self.players, self.round)
