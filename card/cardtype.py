from enum import Enum


class CardType(Enum):
    SVRSEK = 0
    MENIC = 1
    KRAL = 2
    ESO = 3
    SEVEN = 4
    EIGHT = 5
    NINE = 6
    X = 7
