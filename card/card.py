from card.cardcolor import CardColor
from card.cardtype import CardType


class Card:
    def __init__(self, color: CardColor, type: CardType):
        self.color = color
        self.type = type
        self.is_special = self.is_special(type)
        self.is_super_special = self.is_super_special(type)

    @staticmethod
    def is_special(type) -> bool:
        return (CardType.ESO == type) or (CardType.SEVEN == type)

    @staticmethod
    def is_super_special(type: CardType) -> bool:
        return CardType.MENIC == type

    def compare(self, other):
        if isinstance(other, Card):
            return (other.type == self.type) or (other.color == self.color)
        else:
            return False

